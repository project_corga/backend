# Backend

There is a corresponding frontend project. The source code is available here: https://gitlab.com/project_corga/frontend

## Requirements

- IntelliJ IDE

## Development server

- How to correctly open the project with IntelliJ
    - Click File > Open
    - Find backend project in file browser
    - Select build.gradle.kts > OK
    - Open as project


- How to run Application
    - Run src/main/kotlin/de.corga/Application.kts
    - this will generate:
        - MySql database file "~/corag-sql"
        - example data

If you are using the frontend navigate to `http://localhost:4200/`.
