package de.corga.sections

import de.corga.createRequest
import de.corga.createUrl
import de.corga.getListType
import de.corga.sections.dto.SectionCreateInput
import de.corga.sections.dto.SectionOutput
import de.corga.sections.dto.SectionUpdateInput
import de.corga.sections.entities.Section
import de.corga.sections.repositories.SectionRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ActiveProfiles("test")
@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
internal class SectionIntegrationTest {

	@LocalServerPort
	private lateinit var port: Number

	@Autowired
	private lateinit var tester: TestRestTemplate

	@Autowired
	private lateinit var sections: SectionRepository

	private lateinit var root: Section

	@BeforeEach
	fun beforeEach() {
		root = sections.saveAndFlush(Section(name = "root", favorite = false, parent = null))
	}

	@AfterEach
	fun afterEach() {
		sections.deleteAll()
	}

	@Test
	fun list_sections_successfully() {
		val response = tester.exchange(
			createSectionUrlWithPort(),
			HttpMethod.GET,
			createRequest(),
			getListType(SectionOutput::class.java)
		)

		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(listOf(SectionOutput(root)))
	}

	@Test
	fun list_sections_and_filter_by_id() {
		val child = sections.save(Section(parent = root, name = "child", favorite = true))

		checkAssertListSectionsWithPort("?id=${root.id}", child);
	}

	@Test
	fun list_sections_and_page_response() {
		val child = sections.save(Section(parent = root, name = root.name + "'s child", favorite = false))
		checkAssertListSectionsWithPort("?page=1&size=1&sort=name,DESC", child)
	}

	@Test
	fun create_section_successfully() {
		val body = SectionCreateInput(name = "child", favorite = false, parentId = root.id)

		val response = tester.exchange(
			createSectionUrlWithPort(),
			HttpMethod.POST,
			createRequest(body),
			SectionOutput::class.java
		)

		val created = sections.findFirstByName(body.name).orElse(null)

		assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
		assertThat(response.body).isEqualTo(SectionOutput(created))
		assertThatSectionIsCreatedCorrectly(created, body)
	}

	@Test
	fun create_section_without_parent_id_returns_error() {
		val body = """{ "name":"child", "favorite":false}"""

		val response = tester.exchange(
			createSectionUrlWithPort(),
			HttpMethod.POST,
			createRequest(body),
			String::class.java
		)

		assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
	}

	@Test
	fun create_section_with_invalid_parent_id_returns_error() {
		val body = SectionCreateInput(name = "child", favorite = false, parentId = UUID.randomUUID())

		val response = tester.exchange(
			createSectionUrlWithPort(),
			HttpMethod.POST,
			createRequest(body),
			String::class.java
		)

		val created = sections.findFirstByName(body.name).orElse(null)

		assertThat(created).isNull()
		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}

	@Test
	fun read_section_successfully() {
		val response = getHttpStatus(root.id)
		checkHttpStatusOk(response)

		assertThat(response.body).isEqualTo(SectionOutput(root))
	}

	@Test
	fun read_section_includes_subsection() {
		val child = sections.save(Section(parent = root, name = "child", favorite = false))

		val response = getHttpStatus(root.id)
		checkHttpStatusOk(response)

		assertThat(response.body?.subSectionIds).hasSameElementsAs(listOf(child.id))
	}

	@Test
	fun read_section_with_invalid_id_returns_error() {
		val response = tester.exchange(
			createSectionUrlWithPort(UUID.randomUUID()),
			HttpMethod.GET,
			createRequest(),
			String::class.java
		)

		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}

	@Test
	fun update_section_successfully() {
		val body = SectionUpdateInput(name = "new name", favorite = !root.favorite)

		val response = tester.exchange(
			createSectionUrlWithPort(root.id),
			HttpMethod.PATCH,
			createRequest(body),
			SectionOutput::class.java
		)

		val updated = sections.findById(root.id).orElse(null)

		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(SectionOutput(updated))
		assertThatSectionIsUpdatedCorrectly(updated, root, body)
	}

	@Test
	fun update_section_with_invalid_id_returns_error() {
		val body = SectionUpdateInput()

		val response = tester.exchange(
			createSectionUrlWithPort(UUID.randomUUID()),
			HttpMethod.PATCH,
			createRequest(body),
			String::class.java
		)

		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}

	@Test
	fun delete_section_with_wrong_id_returns_error() {
		val response = tester.exchange(
			createSectionUrlWithPort(UUID.randomUUID()),
			HttpMethod.DELETE,
			createRequest(),
			String::class.java
		)

		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}

	private fun createSectionUrlWithPort(): String {
		return createUrl(port, "sections")
	}

	private fun createSectionUrlWithPort(id: UUID): String {
		return createUrl(port, "sections", id)
	}

	private fun assertThatSectionIsCreatedCorrectly(actual: Section, input: SectionCreateInput) {
		assertThat(actual.id).isNotNull()
		assertThat(actual.name).isEqualTo(input.name)
		assertThat(actual.favorite).isEqualTo(input.favorite)
		assertThat(actual.parent?.id).isEqualTo(input.parentId)
		assertThat(actual.children).isEmpty()
		assertThat(actual.cards).isEmpty()
	}

	private fun assertThatSectionIsUpdatedCorrectly(
		actual: Section,
		original: Section,
		input: SectionUpdateInput
	) {
		assertThat(actual.id).isEqualTo(original.id)
		assertThat(actual.name).isEqualTo(input.name)
		assertThat(actual.favorite).isEqualTo(input.favorite)
		assertThat(actual.parent?.id).isEqualTo(original.parent?.id)
		assertThat(actual.children).hasSameElementsAs(original.children)
		assertThat(actual.cards).hasSameElementsAs(original.cards)
	}

	private fun checkAssertListSectionsWithPort(port: String, child:Section){

		val response = tester.exchange(
			createSectionUrlWithPort() + port,
			HttpMethod.GET,
			createRequest(),
			getListType(SectionOutput::class.java)
		)

		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)

		val expected = root.copy(children = listOf(child))
		assertThat(response.body).isEqualTo(listOf(SectionOutput(expected)))
	}
	private fun checkHttpStatusOk(response:ResponseEntity<SectionOutput>)
	{
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
	}

	private fun getHttpStatus(port: UUID):
			ResponseEntity<SectionOutput> {
		return tester.exchange(
			createSectionUrlWithPort(port),
			HttpMethod.GET,
			createRequest(),
			SectionOutput::class.java
		);
	}

}