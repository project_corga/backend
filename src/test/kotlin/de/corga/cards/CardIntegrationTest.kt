package de.corga.cards


import de.corga.cards.dto.CardCreateInput
import de.corga.cards.dto.CardOutput
import de.corga.cards.dto.CardUpdateInput
import de.corga.cards.entities.Card
import de.corga.cards.repositories.CardRepository
import de.corga.createRequest
import de.corga.createUrl
import de.corga.getListType
import de.corga.sections.dto.SectionOutput
import de.corga.sections.entities.Section
import de.corga.sections.repositories.SectionRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*


@ActiveProfiles("test")
@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
internal class CardIntegrationTest {
	
	@LocalServerPort
	private lateinit var port: Number
	
	@Autowired
	private lateinit var tester: TestRestTemplate
	
	@Autowired
	private lateinit var sectionRepository: SectionRepository
	
	@Autowired
	private lateinit var cardRepository: CardRepository
	
	private lateinit var root: Section
	private lateinit var card: Card
	
	@BeforeEach
	fun beforeEach() {
		root = sectionRepository.saveAndFlush(Section(name = "root", favorite = false, parent = null))
		card = cardRepository.saveAndFlush(Card(name = "card", favorite = false, section = root))
	}
	
	@AfterEach
	fun afterEach() {
		sectionRepository.deleteAll()
		cardRepository.deleteAll()
	}
	
	@Test
	fun list_cards_successfully() {
		val response = tester.exchange(
			createCardUrlWithPort(),
			HttpMethod.GET,
			createRequest(),
			getListType(CardOutput::class.java)
		)
		
		val expected = listOf(CardOutput(card))
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(expected)
	}
	
	
	@Test
	fun list_cards_and_filter_by_id() {
		cardRepository.save(Card(section = root, favorite = false, name = "card2"))
		
		val response = tester.exchange(
			createCardUrlWithPort() + "?id=${card.id}",
			HttpMethod.GET,
			createRequest(),
			getListType(CardOutput::class.java)
		)
		
		val expected = listOf(CardOutput(card))
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(expected)
	}
	
	
	@Test
	fun list_cards_and_page_response() {
		cardRepository.save(Card(section = root, name = card.name + "2", favorite = false))
		
		val response = tester.exchange(
			createCardUrlWithPort() + "?page=1&size=1&sort=name,DESC",
			HttpMethod.GET,
			createRequest(),
			getListType(CardOutput::class.java)
		)
		
		val expected = listOf(CardOutput(card))
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(expected)
	}
	
	
	@Test
	fun create_card_successfully() {
		val body = CardCreateInput(
			name = card.name + "2",
			favorite = !Card().favorite,
			sectionId = root.id,
			content = "card content"
		)
		
		val response = tester.exchange(
			createCardUrlWithPort(),
			HttpMethod.POST,
			createRequest(body),
			CardOutput::class.java
		)
		
		val created = cardRepository.findAll().find { el -> el.name == body.name }!!
		
		assertThatCardIsCreatedCorrectly(created, body)
		assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
		assertThat(response.body).isEqualTo(CardOutput(created))
	}
	
	@Test
	fun create_card_without_content() {
		val body = CardCreateInput(
			name = card.name + "2",
			favorite = !Card().favorite,
			sectionId = root.id
		)
		
		val response = tester.exchange(
			createCardUrlWithPort(),
			HttpMethod.POST,
			createRequest(body),
			CardOutput::class.java
		)
		
		val created = cardRepository.findAll().find { el -> el.name == body.name }!!
		
		assertThatCardIsCreatedCorrectly(created, body)
		assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
		assertThat(response.body).isEqualTo(CardOutput(created))
	}
	
	@Test
	fun create_card_with_invalid_section_id_returns_error() {
		val body = CardCreateInput(name = card.name + "2", favorite = false, sectionId = UUID.randomUUID())
		
		val response = tester.exchange(
			createCardUrlWithPort(),
			HttpMethod.POST,
			createRequest(body),
			String::class.java
		)
		
		val created = cardRepository.findAll().find { el -> el.name == body.name }
		
		assertThat(created).isNull()
		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}
	
	@Test
	fun read_card_successfully() {
		val response = tester.exchange(
			createCardUrlWithPort(card.id),
			HttpMethod.GET,
			createRequest(),
			CardOutput::class.java
		)
		
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(CardOutput(card))
	}
	
	@Test
	fun read_section_and_show_cards_successfully() {
		val sectionResponse = tester.exchange(
			createSectionUrlWithPort(root.id),
			HttpMethod.GET,
			createRequest(),
			SectionOutput::class.java
		)
		
		assertThat(sectionResponse.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(sectionResponse.body?.cardIds).contains(card.id)
	}
	
	@Test
	fun read_card_with_invalid_id_returns_error() {
		val response = tester.exchange(
			createCardUrlWithPort(UUID.randomUUID()),
			HttpMethod.GET,
			createRequest(),
			String::class.java
		)
		
		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}
	
	
	@Test
	fun update_card_successfully() {
		val body = CardUpdateInput(name = "new name", favorite = true, content = "new content")
		
		val response = tester.exchange(
			createCardUrlWithPort(card.id),
			HttpMethod.PATCH,
			createRequest(body),
			CardOutput::class.java
		)
		
		val updated = cardRepository.findById(card.id).orElse(null)
		
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(CardOutput(updated))
		assertThatCardIsUpdatedCorrectly(updated, card, body)
	}
	
	@Test
	fun update_card_with_invalid_id_returns_error() {
		val body = CardUpdateInput()
		
		val response = tester.exchange(
			createCardUrlWithPort(UUID.randomUUID()),
			HttpMethod.PATCH,
			createRequest(body),
			String::class.java
		)
		
		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}
	
	@Test
	fun delete_cards_linked_to_section_on_delete_section() {
		val response = tester.exchange(
			createSectionUrlWithPort(root.id),
			HttpMethod.DELETE,
			createRequest(),
			SectionOutput::class.java
		)
		
		assertThat(cardRepository.count()).isEqualTo(0)
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
	}
	
	@Test
	fun delete_card_successfully() {
		val response = tester.exchange(
			createCardUrlWithPort(card.id),
			HttpMethod.DELETE,
			createRequest(),
			CardOutput::class.java
		)
		
		assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(response.body).isEqualTo(CardOutput(card))
	}
	
	
	@Test
	fun delete_card_with_invalid_id_returns_error() {
		val response = tester.exchange(
			createCardUrlWithPort(UUID.randomUUID()),
			HttpMethod.DELETE,
			createRequest(),
			String::class.java
		)
		
		assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
	}
	
	private fun createCardUrlWithPort(): String {
		return createUrl(port, "cards")
	}
	
	private fun createCardUrlWithPort(id: UUID): String {
		return createUrl(port, "cards", id)
	}
	
	private fun createSectionUrlWithPort(id: UUID): String {
		return createUrl(port, "sections", id)
	}
	
	private fun assertThatCardIsCreatedCorrectly(actual: Card, expected: CardCreateInput) {
		assertThat(actual.id).isNotNull()
		assertThat(actual.name).isEqualTo(expected.name)
		assertThat(actual.favorite).isEqualTo(expected.favorite)
		assertThat(actual.content).isEqualTo(expected.content ?: Card().content)
		assertThat(actual.section.id).isEqualTo(expected.sectionId)
	}
	
	private fun assertThatCardIsUpdatedCorrectly(actual: Card, original: Card, input: CardUpdateInput) {
		assertThat(actual.id).isEqualTo(original.id)
		assertThat(actual.name).isEqualTo(input.name)
		assertThat(actual.favorite).isEqualTo(input.favorite)
		assertThat(actual.content).isEqualTo(input.content)
		assertThat(actual.section.id).isEqualTo(original.section.id)
	}
}
