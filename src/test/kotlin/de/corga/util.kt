package de.corga

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.lang3.reflect.TypeUtils.parameterize
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import java.lang.reflect.Type
import java.util.*


fun createUrl(port: Number, type: String): String {
	return "http://localhost:$port/api/v1/$type"
}

fun createUrl(port: Number, type: String, id: UUID): String {
	return "${createUrl(port, type)}/$id"
}

fun createHeaders(): HttpHeaders {
	val headers = HttpHeaders()
	headers.contentType = MediaType.APPLICATION_JSON
	headers.accept = listOf(MediaType.APPLICATION_JSON)
	return headers
}

fun createRequest(body: Any = "", headers: HttpHeaders = createHeaders()) =
	HttpEntity(ObjectMapper().writeValueAsString(body), headers)

fun <T : Type> getListType(type: T): ParameterizedTypeReference<List<T>> {
	return ParameterizedTypeReference.forType(parameterize(List::class.java, type))
}
