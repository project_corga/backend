package de.corga

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories("de.corga.cards")
class Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
