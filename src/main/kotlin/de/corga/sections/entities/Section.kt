package de.corga.sections.entities

import de.corga.cards.entities.Card
import de.corga.sections.dto.SectionCreateInput
import java.util.*
import javax.persistence.*

@Entity
data class Section(
	@Id @GeneratedValue
	val id: UUID = UUID.randomUUID(),
	var name: String = "",
	var favorite: Boolean = false,
	@ManyToOne(fetch = FetchType.LAZY)
	var parent: Section? = null,
	@OneToMany(mappedBy = "parent", cascade = [CascadeType.REMOVE], orphanRemoval = true, fetch = FetchType.LAZY)
	var children: Collection<Section> = listOf(),
	@OneToMany(mappedBy = "section", cascade = [CascadeType.REMOVE], orphanRemoval = true, fetch = FetchType.LAZY)
	var cards: Collection<Card> = listOf()
) {
	constructor(parent: Section, dto: SectionCreateInput) : this(
		name = dto.name,
		favorite = dto.favorite,
		parent = parent
	)
}
