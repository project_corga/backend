package de.corga.sections.repositories

import de.corga.sections.entities.Section
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SectionRepository : JpaRepository<Section, UUID>, JpaSpecificationExecutor<Section> {
	fun findFirstByName(name: String): Optional<Section>
}
