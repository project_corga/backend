package de.corga.sections.controller

import de.corga.sections.dto.SectionCreateInput
import de.corga.sections.dto.SectionListInput
import de.corga.sections.dto.SectionOutput
import de.corga.sections.dto.SectionUpdateInput
import de.corga.sections.entities.Section
import de.corga.sections.services.SectionService
import net.kaczmarzyk.spring.data.jpa.domain.Equal
import net.kaczmarzyk.spring.data.jpa.domain.In
import net.kaczmarzyk.spring.data.jpa.web.annotation.And
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("api/v1/sections")
class SectionController(
	private val service: SectionService
) {
	
	@GetMapping("")
	fun list(
		@And(
			Spec(path = "id", spec = Equal::class),
			Spec(path = "name", spec = In::class),
			Spec(path = "parent.id", params = ["parentId"], spec = Equal::class)
		) specification: Specification<Section>? = null,
		@PageableDefault(page = 0, size = 100)
		pagination: Pageable = Pageable.unpaged()
	): List<SectionOutput> {
		return service.list(SectionListInput(pagination, specification))
	}
	
	@PostMapping("")
	@ResponseStatus(HttpStatus.CREATED)
	fun create(@RequestBody @Validated content: SectionCreateInput): SectionOutput {
		return service.create(content)
	}
	
	@GetMapping("/{id}")
	fun read(@PathVariable id: UUID): SectionOutput {
		return service.read(id)
	}
	
	@DeleteMapping("/{id}")
	fun delete(@PathVariable id: UUID): SectionOutput {
		return service.delete(id)
	}
	
	@PatchMapping("/{id}")
	fun update(
		@PathVariable id: UUID,
		@RequestBody @Validated content: SectionUpdateInput
	): SectionOutput {
		return service.update(id, content)
	}
}
