package de.corga.sections

import javassist.NotFoundException

class SectionNotFoundException(message: String = "") : NotFoundException(message)
