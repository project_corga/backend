package de.corga.sections.dto

data class SectionUpdateInput(
	val name: String? = null,
	val favorite: Boolean? = null
)
