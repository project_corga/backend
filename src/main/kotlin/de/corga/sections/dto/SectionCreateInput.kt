package de.corga.sections.dto

import java.util.*

data class SectionCreateInput(
	val name: String,
	val parentId: UUID,
	val favorite: Boolean
)
