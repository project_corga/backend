package de.corga.sections.dto

import de.corga.sections.entities.Section
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification

data class SectionListInput(
	val pagination: Pageable,
	val specification: Specification<Section>? = null
)
