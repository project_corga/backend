package de.corga.sections.dto

import de.corga.sections.entities.Section
import java.util.*

data class SectionOutput(
	val id: UUID,
	val name: String,
	val parentId: UUID?,
	val favorite: Boolean,
	val cardIds: Collection<UUID>,
	val subSectionIds: Collection<UUID>
) {
	constructor(data: Section) : this(
		data.id,
		data.name,
		data.parent?.id,
		data.favorite,
		data.cards.map { it.id },
		data.children.map { it.id })
}
