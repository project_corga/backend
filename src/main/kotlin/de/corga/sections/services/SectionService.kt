package de.corga.sections.services

import de.corga.sections.SectionNotFoundException
import de.corga.sections.dto.SectionCreateInput
import de.corga.sections.dto.SectionListInput
import de.corga.sections.dto.SectionOutput
import de.corga.sections.dto.SectionUpdateInput
import de.corga.sections.entities.Section
import de.corga.sections.repositories.SectionRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class SectionService(
	private val sections: SectionRepository
) {
	fun create(dto: SectionCreateInput): SectionOutput {
		val parent = sections.findById(dto.parentId).orElseThrow { SectionNotFoundException("parent not found") }
		val obj = sections.save(Section(parent, dto))
		return SectionOutput(obj)
	}
	
	fun delete(id: UUID): SectionOutput {
		val obj = sections.findById(id).orElseThrow { SectionNotFoundException() }
		sections.delete(obj)
		return SectionOutput(obj)
	}
	
	fun update(id: UUID, dto: SectionUpdateInput): SectionOutput {
		val original = sections.findById(id).orElseThrow { SectionNotFoundException() }
		val updated = original.copy(
			name = dto.name ?: original.name,
			favorite = dto.favorite ?: original.favorite
		)
		val saved = sections.save(updated)
		return SectionOutput(saved)
	}
	
	fun read(id: UUID): SectionOutput {
		val obj = sections.findById(id).orElseThrow { SectionNotFoundException() }
		return SectionOutput(obj)
	}
	
	fun list(dto: SectionListInput): List<SectionOutput> {
		val obj = sections.findAll(dto.specification, dto.pagination)
		return obj.content.map(::SectionOutput)
	}
	
	fun createRootSection(): Section {
		val obj = Section(
			name = "root",
			parent = null
		)
		return sections.save(obj)
	}
}
