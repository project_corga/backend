package de.corga.cards.controller

import de.corga.cards.dto.CardCreateInput
import de.corga.cards.dto.CardListInput
import de.corga.cards.dto.CardOutput
import de.corga.cards.dto.CardUpdateInput
import de.corga.cards.entities.Card
import de.corga.cards.services.CardService
import net.kaczmarzyk.spring.data.jpa.domain.Equal
import net.kaczmarzyk.spring.data.jpa.domain.In
import net.kaczmarzyk.spring.data.jpa.web.annotation.And
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("api/v1/cards")
class CardController(
	private val service: CardService
) {
	
	@GetMapping("")
	fun list(
		@And(
			Spec(path = "id", spec = Equal::class),
			Spec(path = "name", spec = In::class),
			Spec(path = "section.id", params = ["sectionId"], spec = Equal::class)
		) specification: Specification<Card>? = null,
		@PageableDefault(page = 0, size = 100)
		pagination: Pageable = Pageable.unpaged()
	): List<CardOutput> {
		return service.list(CardListInput(pagination, specification))
	}
	
	@PostMapping("")
	@ResponseStatus(HttpStatus.CREATED)
	fun create(@RequestBody @Validated content: CardCreateInput): CardOutput {
		return service.create(content)
	}
	
	@GetMapping("/{id}")
	fun read(@PathVariable id: UUID): CardOutput {
		return service.read(id)
	}
	
	@DeleteMapping("/{id}")
	fun delete(@PathVariable id: UUID): CardOutput {
		return service.delete(id)
	}
	
	@PatchMapping("/{id}")
	fun update(
		@PathVariable id: UUID,
		@RequestBody @Validated content: CardUpdateInput
	): CardOutput {
		return service.update(id, content)
	}
}
