package de.corga.cards.services

import de.corga.cards.CardNotFoundException
import de.corga.cards.dto.CardCreateInput
import de.corga.cards.dto.CardListInput
import de.corga.cards.dto.CardOutput
import de.corga.cards.dto.CardUpdateInput
import de.corga.cards.entities.Card
import de.corga.cards.repositories.CardRepository
import de.corga.sections.SectionNotFoundException
import de.corga.sections.repositories.SectionRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class CardService(
	private val sections: SectionRepository,
	private val cards: CardRepository
) {
	fun create(dto: CardCreateInput): CardOutput {
		val parent = sections.findById(dto.sectionId).orElseThrow { SectionNotFoundException("parent not found") }
		val obj = cards.save(Card(parent, dto))
		return CardOutput(obj)
	}
	
	fun delete(id: UUID): CardOutput {
		val obj = cards.findById(id).orElseThrow { CardNotFoundException() }
		cards.delete(obj)
		return CardOutput(obj)
	}
	
	fun update(id: UUID, dto: CardUpdateInput): CardOutput {
		val original = cards.findById(id).orElseThrow { CardNotFoundException() }
		val updated = original.copy(
			name = dto.name ?: original.name,
			favorite = dto.favorite ?: original.favorite,
			content = dto.content ?: original.content
		)
		val saved = cards.save(updated)
		return CardOutput(saved)
	}
	
	fun read(id: UUID): CardOutput {
		val obj = cards.findById(id).orElseThrow { CardNotFoundException() }
		return CardOutput(obj)
	}
	
	fun list(dto: CardListInput): List<CardOutput> {
		val objects = cards.findAll(dto.specification, dto.pagination)
		return objects.content.map(::CardOutput)
	}
}
