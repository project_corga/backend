package de.corga.cards.dto

import de.corga.cards.entities.Card
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification

data class CardListInput(
	val pagination: Pageable,
	val specification: Specification<Card>? = null
)
