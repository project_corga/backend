package de.corga.cards.dto

import de.corga.cards.entities.Card
import java.util.*

data class CardOutput(
	val id: UUID,
	val name: String,
	val favorite: Boolean,
	val sectionId: UUID,
	val content: String
) {
	constructor(data: Card) : this(data.id, data.name, data.favorite, data.section.id, data.content)
}
