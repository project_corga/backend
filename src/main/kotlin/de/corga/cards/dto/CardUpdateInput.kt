package de.corga.cards.dto

data class CardUpdateInput(
	val name: String? = null,
	val favorite: Boolean? = null,
	val content: String? = null
)
