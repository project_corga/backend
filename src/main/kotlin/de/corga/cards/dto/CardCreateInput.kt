package de.corga.cards.dto

import java.util.*

data class CardCreateInput(
	val name: String,
	val favorite: Boolean,
	val sectionId: UUID,
	val content: String? = null
)
