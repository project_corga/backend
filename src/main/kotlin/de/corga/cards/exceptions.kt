package de.corga.cards

import javassist.NotFoundException

class CardNotFoundException(message: String = "") : NotFoundException(message)