package de.corga.cards.entities

import de.corga.cards.dto.CardCreateInput
import de.corga.sections.entities.Section
import java.util.*
import javax.persistence.*

@Entity
data class Card(
	@Id
	@GeneratedValue
	val id: UUID = UUID.randomUUID(),
	var name: String = "",
	var favorite: Boolean = false,
	@ManyToOne(fetch = FetchType.LAZY)
	var section: Section = defaultSection(),
	@Column( length = 100000 )
	var content: String = ""
) {
	constructor(section: Section, dto: CardCreateInput) : this(
		name = dto.name,
		favorite = dto.favorite,
		section = section,
		content = dto.content ?: ""
	)
}

private fun defaultSection(): Section {
	return Section(
		name = "default",
		favorite = false
	)
}
