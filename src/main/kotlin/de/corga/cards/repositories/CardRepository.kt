package de.corga.cards.repositories

import de.corga.cards.entities.Card
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CardRepository : JpaRepository<Card, UUID>, JpaSpecificationExecutor<Card> {
	fun findFirstByName(name: String): Optional<Card>
}