package de.corga

import de.corga.cards.dto.CardCreateInput
import de.corga.cards.services.CardService
import de.corga.sections.dto.SectionCreateInput
import de.corga.sections.services.SectionService
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Profile("!test")
@Component
class DataInitializer(
	private val sectionService: SectionService,
	private val cardService: CardService
) : ApplicationRunner {
	override fun run(args: ApplicationArguments?) {
		val root = sectionService.createRootSection()
		val university = sectionService.create(SectionCreateInput("University", root.id, false))
		val family = sectionService.create(SectionCreateInput("Family", root.id, false))
		val events = sectionService.create(SectionCreateInput("Events", root.id, false))

		val benno = sectionService.create(SectionCreateInput("Dog Benno", family.id, true))
		val anna = sectionService.create(SectionCreateInput("Anna", family.id, false))

		val softwareengineering = sectionService.create(SectionCreateInput("Software Engineering", university.id, true))
		val math = sectionService.create(SectionCreateInput("Math", university.id, false))

		val em = sectionService.create(SectionCreateInput("EM", events.id, false))

		
		cardService.create(CardCreateInput("Buy food", false, benno.id,"* 2 Pounds DoggysBest <br>* 4 Pounds DogDogs CatBurner" ))
		cardService.create(CardCreateInput("Doctors Appointment", false, benno.id,"* June 5th, Doctor Dogwhisperer" ))
		cardService.create(CardCreateInput("Homework", false, anna.id, "* Math till Friday <br>* Biology till Sunday"))
		cardService.create(CardCreateInput("Finals Presentation", false, softwareengineering.id, "* To Do!"))
		cardService.create(CardCreateInput("Betting game", true, em.id, "* Bea: Germany <br>* Katja: Poland<br>* Jonas:England"))
		
	}
}
