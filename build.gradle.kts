import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	java
	kotlin("jvm") version "1.3.70"
	kotlin("plugin.spring") version "1.3.70"
	
	id("org.springframework.boot") version "2.3.3.RELEASE"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
}

group = "de.corga"
version = "1.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

springBoot {
	mainClassName = "de.corga.ApplicationKt"
}

repositories {
	mavenCentral()
}

dependencies {
	implementation(kotlin("stdlib"))
	
	//basic spring boot dependencies
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	
	testImplementation("org.apache.httpcomponents:httpclient:4.5.13")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
	testRuntimeOnly("mysql:mysql-connector-java")
	
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin") //needed to convert request-body to class
	implementation("org.jetbrains.kotlin:kotlin-reflect") //needed to compile JPA-Repository
	runtimeOnly("com.h2database:h2") //configure database
	implementation("net.kaczmarzyk:specification-arg-resolver:2.6.2") // needed for request parameter querying
}

tasks.withType<Test> {
	useJUnitPlatform()
	
	// force eager-loading of entities in tests
	systemProperties["spring.jpa.properties.hibernate.enable_lazy_load_no_trans"] = true
	
	testLogging {
		events = setOf(
			TestLogEvent.FAILED,
			TestLogEvent.STANDARD_OUT
		)
		
		addTestListener(TestResultConsoleLogger())
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

class TestResultConsoleLogger : TestListener {
	override fun beforeSuite(suite: TestDescriptor?) {
	}
	
	override fun afterSuite(suite: TestDescriptor?, result: TestResult) {
		if (suite?.parent == null) {
			val output = "Results: ${result.resultType} (${result.testCount} tests, " +
				"${result.successfulTestCount} passed, " +
				"${result.failedTestCount} failed, " +
				"${result.skippedTestCount} skipped)"
			println(convertToBox(output))
		}
	}
	
	override fun beforeTest(testDescriptor: TestDescriptor?) {
	}
	
	override fun afterTest(testDescriptor: TestDescriptor?, result: TestResult?) {
	}
	
	private fun convertToBox(message: String): String {
		val startItem = "|  "
		val endItem = "  |"
		val repeatLength = startItem.length + message.length + endItem.length
		
		return "\n" + ("-".repeat(repeatLength)) +
			"\n" + startItem + message + endItem + "\n" +
			("-".repeat(repeatLength)) + "\n"
	}
}
